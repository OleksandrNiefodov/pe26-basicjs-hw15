/*
Теоретичні питання
1. В чому відмінність між setInterval та setTimeout?

- setInterval запускає якусь функцію через заданий інтервал часу. Він буде виконуватись безкінчечно якщо його не зупинити.
- setTimeout запускає функцію чере інтервал часу один раз

2. Як припинити виконання функції, яка була запланована для виклику з використанням setTimeout та setInterval?

- для setTimeout використати метод clearTimeout(), а для setInterval метод clearInterval()

Практичне завдання:
-Створіть HTML-файл із кнопкою та елементом div.
-При натисканні кнопки використовуйте setTimeout, щоб змінити текстовий вміст елемента div через затримку 3 секунди. Новий текст повинен вказувати, що операція виконана успішно.
*/
    const button = document.querySelector('#button');
    const div = document.querySelector('#js-div');

    button.addEventListener('click', () => {
      setTimeout(() => {
        div.textContent = 'Success';
      }, 3000);
    });
  

/*
Практичне завдання 2:
Реалізуйте таймер зворотного відліку, використовуючи setInterval. При завантаженні сторінки виведіть зворотний відлік від 10 до 1 в елементі div. Після досягнення 1 змініть текст на "Зворотній відлік завершено".
*/


  const divElement = document.querySelector('#div');

  const countdown = setInterval(() => {
  
    divElement.textContent = --divElement.textContent;

    if (divElement.textContent === '1') {
      clearInterval(countdown);
      divElement.textContent = 'Countdown is complete';
    }
  }, 1000);

